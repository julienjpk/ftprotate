#!/usr/bin/env python3

# ftprotate - a tool to delete old files from remote FTP servers
# Copyright (C) 2019 Julien JPK <julienjpk@email.com>

# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.

# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details.

# You should have received a copy of the GNU Affero General Public License along
# with this program. If not, see <https://www.gnu.org/licenses/>.

from setuptools import setup, find_packages

setup(
    name='ftprotate',
    version='1.0.0.dev1',
    description='A tool to delete old files from remote FTP servers',
    author='Julien JPK',
    author_email='julienjpk@email.com',
    license='AGPL-3.0',
    packages=find_packages(),
    install_requires=[],
    entry_points={
        'console_scripts': [
            'ftprotate = ftprotate.__main__:main'
        ]
    }
)
