# Copyright (C) 2019 Julien JPK <julienjpk@email.com>

# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.

# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details.

# You should have received a copy of the GNU Affero General Public License along
# with this program. If not, see <https://www.gnu.org/licenses/>.

from urllib.parse import urlparse
from netrc import netrc

import datetime as dt
import argparse
import ftplib
import sys


def rotate_file(ftp, directory, name, modify, fmt, days):
    """
    Determines whether or not a file has to be deleted, and does so if needed
    :param ftp: the FTP server
    :param directory: the current directory
    :param name: the file to be evaluated
    :param modify: the file's timestamp as reported by the FTP server
    :param fmt: the format to use if parsing dates from file names
    :param days: the number of days after which a file should be deleted
    :return: None
    """
    if fmt is None:
        dot_at = modify.find('.')
        if dot_at > 0:
            modify = modify[:dot_at]
        ts = dt.datetime.strptime(modify, '%Y%m%d%H%M%S')
    else:
        try:
            ts = dt.datetime.strptime(name, fmt)
        except ValueError:
            return

    now = dt.datetime.utcnow()
    if now > ts and now - ts >= dt.timedelta(days=days):
        ftp.delete(directory + '/' + name)


def rotate_directory(ftp, directory, fmt, days):
    """
    Processes a single directory
    :param ftp: the FTP server
    :param directory: the current directory
    :param fmt: the format to use if parsing dates from file names
    :param days: the number of days after which a file should be deleted
    :return: None
    """
    if directory == '/':
        directory = ''
    for name, facts in ftp.mlsd(directory):
        if facts['type'] == 'dir':
            rotate_directory(ftp, directory + '/' + name, fmt, days)
        if facts['type'] == 'file':
            rotate_file(ftp, directory, name, facts['modify'], fmt, days)


def main():
    # Parse arguments
    parser = argparse.ArgumentParser(description='Rotate FTP server files')
    parser.add_argument('url', type=str,
                        help='the FTP directory URL to process')
    parser.add_argument('days', type=int,
                        help='the number of days after which a file should be '
                             'deleted')
    parser.add_argument('-f', '--format',
                        help='an strptime-compatible format for timestamp '
                             'parsing from filenames')
    parser.add_argument('-a', '--anon-passwd',
                        help='the password to use for anonymous authentication')
    args = parser.parse_args()

    # Attempt to parse the FTP url
    try:
        ftp_url = urlparse(args.url)
        _ = ftp_url.port  # validating the port while we're at it
    except ValueError:
        print("%s: invalid URL: %s" % (sys.argv[0], args.url))
        return 1

    # Acquire credentials from the netrc file
    try:
        auth = netrc().authenticators(ftp_url.hostname)
    except (FileNotFoundError, PermissionError) as e:
        print("%s: cannot access your netrc file: %s" % (sys.argv[0], str(e)))
        return 1

    # Fallback to anonymous login if needed
    if auth is None:
        username = "anonymous"
        password = "guest" if args.anon_passwd is None else args.anon_passwd
    else:
        username = auth[0]
        password = auth[2]

    # Establish a connection and login
    try:
        ftp = ftplib.FTP()
        ftp.connect(ftp_url.hostname, ftp_url.port)
        ftp.login(username, password)
    except ftplib.all_errors as e:
        print("%s: FTP connection error: %s" % (sys.argv[0], str(e)))
        return 1

    # Start at the root and down we go!
    try:
        rotate_directory(ftp, ftp_url.path, args.format, args.days)
    except ftplib.all_errors as e:
        print("%s: FTP processing error: %s" % (sys.argv[0], str(e)))
        return 1

    ftp.close()
    return 0


if __name__ == '__main__':
    sys.exit(main())
